#!/bin/bash

#Stop and delete containerss
./stop.sh
#delete image and volume
docker rmi tp5_efsh_image
docker volume rm tp5_efsh_vol

#rebuild image and volume
docker-compose -f project/docker/docker-compose.yml --project-directory . build
docker volume create --name tp5_efsh_vol --opt device=$PWD --opt o=bind --opt type=none
echo "volume created"
#Run container
docker-compose -f project/docker/docker-compose.yml --project-directory . up -d
echo "docker running"
#docker exec -it tp5_efsh bin/bash