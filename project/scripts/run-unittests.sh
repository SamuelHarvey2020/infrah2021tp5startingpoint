#!/bin/bash

echo "Running unittest"
coverage run --omit */test_*,*__init__.py,/usr/lib/python3/dist-packages/blinker/base.py -m unittest discover -s ./project -v -p test_*.py
if [ $? -ne 0 ]
then
	echo "unittest failed, aborting"
	exit 1
fi

echo "Generating coverage report"
coverage report