/* SAMUEL :  Ce fichier doit être passé au conteneur (au bon endroit) et sera exécuté automatiquement lors du lancement du conteneur. Vous 
trouverez plus d’informations à cet effet sur la page de description de l’image mysql (https://hub.docker.com/_/mysql). */

CREATE DATABASE usersDB;
use usersDB;

CREATE TABLE users (
	username VARCHAR(32) NOT NULL,
	isInitialUser BIT DEFAULT 0 NOT NULL,
	UNIQUE(username)
);

/* SAMUEL : TODO : AJOUTER REQUETTES SQL POUR POUR INSERER LES USAGERS [ POUR LES TESTS ]
		
		USAGERS A CRÉÉR :  Bob*, Bill, Body*, Joe, Jack (* = InitialUsers = 1)
*/
INSERT INTO users (username, isInitialUser)
VALUES 
		(Bob, 1),
		(Bill, 0),
		(Body, 1),
		(Joe, 0),
		(Jack, 0);