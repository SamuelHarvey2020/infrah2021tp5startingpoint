#!/bin/bash

echo "Deploying app in docker locally"
./deployLocal.sh

echo "Running deployment tests"
python3 -m unittest discover -s project -v -p dtest_*.py