#!/bin/bash

#stop container
docker stop tp5_efsh

#delete container
docker rm tp5_efsh -v

#delete image and volume
docker rmi tp5_efsh_image
docker volume rm tp5_efsh_vol

#rebuild image and volume
docker volume create --name tp5_efsh_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp5_efsh_image -f ./project/docker/DockerfileAPI .

#Run container
docker run -p 5555:5555 --mount source=tp5_efsh_vol,target=/mnt/app/ -e INFRA_TP5_DB_TYPE=MYSQL -e INFRA_TP5_DB_HOST=db-tp5-infra.ddnsgeek.com -e INFRA_TP5_DB_PORT=7777 -e INFRA_TP5_DB_PASSWORD=3ac4d0b0e24871436f45275890a458c6 -e INFRA_TP5_DB_USER=produser --name tp5_efsh tp5_efsh_image 
#docker exec -it tp3efsh bin/bash
